#!/usr/bin/env bash

debug_log ()
{
  verbose="${verbose:="false"}"
  local msg="$*"
  if [[ "$verbose" == "true" ]]; then
    printf "$msg"
  fi
}

process_args ()
{
  local args=("$@")
  local arg
  for arg in ${args[@]}
  do
    case "$arg" in
      "-v"|"--verbose")
        verbose="true"
        debug_log "Verbose mode enabled.\n"
      ;;
    esac
  done

  local arg_index
  for arg_index in ${!args[@]}
  do
    arg=${args[$arg_index]}
    case "$arg" in
      "-t"|"--target="*)
        debug_log "Processing target argument...\n"
        local target_arg=$arg
        if [[ "$target_arg" == "-t" ]]; then
          target="${args[((arg_index+1))]}"
        elif [[ "$target_arg" = "--target="* ]]; then
          target="${target_arg#"--target="}"
          target="${target/#\~/$HOME}"
        fi
      ;;
      "-w"|"--workdir="*)
        debug_log "Processing workdir argument...\n"
        local workdir_arg=$arg
        if [[ "$workdir_arg" == "-w" ]]; then
          work_dir="${args[((arg_index+1))]}"
        elif [[ "$workdir_arg" == "--workdir="* ]]; then
          work_dir="${workdir_arg#"--workdir="}"
          work_dir="${work_dir/#\~/$HOME}"
        fi
        ;;
      "-s"|"--source="*)
        debug_log "Processing source argument...\n"
        local source_arg=$arg
        if [[ "$source_arg" == "-s" ]]; then
          source="${args[((arg_index+1))]}"
        elif [[ "$source_arg" == "--source="* ]]; then
          source="${source_arg#"--source="*}"
        fi
        ;;
    esac
  done
}

check_source ()
{
  source_docker_image="$(tomlq -r .sources.$source.docker_image "$config_file" 2> /dev/null)"
  if [[ -z "$source_docker_image" ]]; then
    debug_log "Source docker image is not specified.[$source]\n"
    return 1
  fi

  source_data="$(tomlq -r .sources.$source.data "$config_file" 2> /dev/null)"
  source_data="${source_data/#\~/$HOME}"
  if [[ -z "$source_data" ]] || [[ ! -d "$source_data" ]]; then
    debug_log "Source data is not specified or invalid.[$source]\n"
    return 2
  fi

  source_config="$(tomlq -r .sources.$source.config "$config_file" 2> /dev/null)"
  source_config="${source_config/#\~/$HOME}"
  if [[ -z "$source_config" ]] || [[ ! -d "$source_config" ]]; then
    debug_log "Source config is not specified or invalid.[$source]\n"
    return 3
  fi

  source_cache="$(tomlq -r .sources.$source.cache "$config_file" 2> /dev/null)"
  source_cache="${source_cache/#\~/$HOME}"
  if [[ -z "$source_cache" ]] || [[ ! -d "$source_cache" ]]; then
    debug_log "Source cache is not specified or invalid.[$source]\n"
    return 4
  fi

  return 0
}

setup ()
{
  config_dir="${XDG_CONFIG_HOME:-"$HOME/.config"}/docked-vim"
  if [[ ! -d "$config_dir" ]]; then
    debug_log "Config directory not found. Creating...[$config_dir]\n"
    mkdir "$config_dir"
  fi

  config_file="$config_dir/docked-vim.toml"
  if [[ ! -f "$config_file" ]]; then
    debug_log "Config file not found.[$config_file]\n"
    debug_log "Exitting...\n"
    exit 1
  fi

  local target_backslashed="false"
  if [[ "$target" == *"/" ]]; then
    target_backslashed="true"
  fi
  target="$(realpath -m "$target" 2> /dev/null)"
  work_dir="$(realpath -m "$work_dir" 2> /dev/null)"

  if [[ -z "$target" ]] && [[ -z "$work_dir" ]]; then
    work_dir="$(pwd)"
    target=$work_dir
    debug_log "Target and working directory is not specified. Using current directory...[$work_dir]\n"
  elif [[ -z "$work_dir" ]]; then
    if [[ -d "$target" ]]; then
      work_dir=$target
      debug_log "Working directory is not specified. Using target...[$work_dir]\n"
    elif [[ -f "$target" ]]; then
      work_dir="$(dirname "$target")"
      debug_log "Working directory is not specified. Using target's directory...[$work_dir]\n"
    elif [[ "$target_backslashed" == "true" ]]; then
      work_dir=$target
      mkdir -p "$target"
      debug_log "Target not found. Creating directory...[$target]\n"
    else
      work_dir="$(dirname "$target")"
      mkdir -p "$work_dir"
      touch "$target"
      debug_log "Target not found. Creating file...[$target]\n"
    fi
  elif [[ -z "$target" ]]; then
    if [[ ! -d "$work_dir" ]]; then
      mkdir -p "$work_dir"
      debug_log "Working directory not found. Creating...[$work_dir]\n"
    fi
    target=$work_dir
    debug_log "Target is not specified. Using working directory...[$work_dir]\n"
  fi
  
  if [[ "$target" != "$work_dir"* ]]; then
    work_dir="$(dirname "$target")"
    debug_log "Working directory is not a superdirectory of target. Using target's directory...[$work_dir]\n"
  fi

  check_source "$source"
  local source_check_result=$?
  if [[ "$source_check_result" != "0" ]]; then
    default_source_fallback="$(tomlq -r .general.default_source_fallback "$config_file" 2> /dev/null)"
    if [[ "$default_source_fallback" == "true" ]]; then
      debug_log "Using default source as fallback...\n"
      default_source="$(tomlq -r .general.default_source "$config_file" 2> /dev/null)"
      source=$default_source
      check_source "$source"
      source_check_result=$?
      if [[ "$source_check_result" != "0" ]]; then
        debug_log "Default source configuration is not valid.[$source]\n"
        debug_log "Exitting...\n"
        exit 3
      fi
    else
      debug_log "Exitting...\n"
      exit 2
    fi
  fi
}

run ()
{
  local docker_data="/home/$USER/.local/share/nvim"
  local docker_config="/home/$USER/.config/nvim"
  local docker_cache="/home/$USER/.cache/nvim"
  local docker_work_dir="/home/$USER/context"

  local relative_target="${target#"$work_dir/"}"

  docker run --rm -it \
    -v "$source_data:$docker_data" \
    -v "$source_config:$docker_config" \
    -v "$source_cache:$docker_cache" \
    -v "$work_dir:$docker_work_dir" \
    "$source_docker_image" nvim "$docker_work_dir/$relative_target"
}

main ()
{
  local positional_args="$@"
  process_args $positional_args
  setup
  run
}

main "$@"
