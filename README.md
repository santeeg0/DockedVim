<h1 align="center">Docked-Vim</h1>

<div align="center">
	<span> • </span>
				<a href="https://codeberg.org/santeeg0/DockedVim#what-is-it">What is it</a>
	<span> • </span>
				<a href="https://codeberg.org/santeeg0/DockedVim#why">Why</a>
	<span> • </span>
				<a href="https://codeberg.org/santeeg0/DockedVim#installation">Installation</a>
	<span> • </span>
				<a href="https://codeberg.org/santeeg0/DockedVim#configuration">Configuration</a>
	<span> • </span>
				<a href="https://codeberg.org/santeeg0/DockedVim#usage">Usage</a>
	<span> • </span>	
</div>

## What is it?
---
- ***Docked-Vim*** is a bash script that simplifies usage of [*NeoVim*](https://neovim.io) inside of [*Docker*](https://www.docker.com).

- ***Docked-Vim*** works with any *Docker Image* equipped with *NeoVim*.

## Why?
---
> Most of *NeoVim* configs requires **plenty of plugins** which in turn may have their own system **dependencies**.

- With ***Docked-Vim*** you dont have to worry about installing **dependencies** on your host system. Anything required for specific config installed **locally** inside *Docker Image.*

> *NeoVim* itself out of the box is **blazing fast**. But installing large number of plugins*(LSP, Linters, Formatters)* **reduces performance** for quite a bit even when all of this functionality is not needed.

- With ***Docked-Vim*** you can have **multiple** *NeoVim* configs to ensure **best performance and optimal workflow** for specific use case.

## Installation
---
### Linux

Download:   
`git clone https://codeberg.org/santeeg0/docked-vim.git`

*Optionally make it globally accessible:*   
`cd docked-vim`   
`ln -s docked-vim.sh ~/.local/bin/dvim`

## Configuration
---
> ***Docked-Vim*** will look for configuration file `~/.config/docked-vim/docked-vim.toml`.

1. Create configuration file if there isn't one:
	1. `mkdir ~/.config/docked-vim`
	2. `touch ~/.config/docked-vim/docked-vim.toml`
2. Populate configuration file with entries of sources with following formatting:
```toml
[sources]
{source_name}.docker_image = "{docker_image_name}"
{source_name}.data = "{absolute_path_to_data}"
{source_name}.config = "{absolute_path_to_config}"
{source_name}.cache = "{absolute_path_to_cache}"
```

Simple example of `docked-vim.toml`:
```toml
[general]
default_source = "dvim_nvchad_basic"
default_source_fallback = "true"

[sources]
dvim_nvchad_basic.docker_image = "santeeg0/dvim-nvchad-basic"
dvim_nvchad_basic.data = "~/data/docker/dvim-nvchad-basic/data"
dvim_nvchad_basic.config = "~/data/docker/dvim-nvchad-basic/config"
dvim_nvchad_basic.cache = "~/data/docker/dvim-nvchad-basic/cache"
```

## Usage

`dvim [-s source] [-w workdir] [-t target]`
or
`dvim [--source=source] [--workdir=workdir] [--target=target]`

Arguments:
- `-s` or `--source=` name of the entry in configuration file;
- `-w` or `--workdir=` path to highest directory that will be accessible in *NeoVim*;
- `-t` or `--target=` path to directory or file that will be opened in *NeoVim*.

### Tips
---
- You can specify environment variable `DVIM_DEFAULT_SOURCE`, then it will be used when `--source` is not provided;
- If `--workdir` and `--target` is not provided, then both of them equals to current directory;
- If `--target` is not provided but `--workdir` is, then `--target` equals to `--workdir`;
- if `--workdir` is not provided but `--target` is, then:
	- `--target` is a directory: `--workdir` equals to `--target`;
	- `--target` is a file: `--workdir` equals to `dirname` of `--target`;

### Examples

`dvim -s santeeg0/neovim-basic --workdir=~/Data/docker`

`dvim --source=santeeg0/neovim-basic -t ~/.config/docked-vim/docked-vim.toml`

`dvim -s santeeg0/neovim-basic`

`dvim --target=./README.md`

`dvim`